import tkinter as tk
from PIL import Image, ImageTk
import io
import requests
import threading
import openai
import base64
from io import BytesIO

class App:
    def __init__(self, root):
        self.root = root

        self.api_key_label = tk.Label(root, text="OpenAI API Key")
        self.api_key_label.pack()
        self.api_key_entry = tk.Entry(root)
        self.api_key_entry.pack()

        self.prompt_label = tk.Label(root, text="Prompt")
        self.prompt_label.pack()
        self.prompt_entry = tk.Entry(root)
        self.prompt_entry.pack()

        self.generate_button = tk.Button(root, text="Generate Image", command=self.fetch_image)
        self.generate_button.pack()

        self.status_label = tk.Label(root, text="")
        self.status_label.pack()

        self.image_label = tk.Label(root)
        self.image_label.pack()

    def fetch_image(self):
        self.status_label['text'] = 'Loading...'
        self.generate_button['state'] = 'disabled'
        api_key = self.api_key_entry.get()
        prompt = self.prompt_entry.get()

        threading.Thread(target=self.update_image, args=(api_key, prompt)).start()

    def update_image(self, api_key, prompt):
        # Set the OpenAI API Key
        openai.api_key = api_key

        # Create an image using DALL-E
        try:
            response = openai.Image.create(
                prompt=prompt,
                n=1,
                size="1024x1024"
            )
            image_url = response['data'][0]['url']
            image_response = requests.get(image_url)
            image_bytes = image_response.content

            self.display_image(image_bytes)
        except openai.error.OpenAIError as e:
            self.status_label['text'] = 'Error: ' + str(e)

    def display_image(self, image_bytes):
        image = Image.open(io.BytesIO(image_bytes))
        photo = ImageTk.PhotoImage(image)

        self.image_label['image'] = photo
        self.image_label.photo = photo
        self.status_label['text'] = ''
        self.generate_button['state'] = 'normal'

root = tk.Tk()
app = App(root)
root.mainloop()
